//
//  CALayer+ProxyProperties.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/1/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "CALayer+ProxyProperties.h"

@implementation CALayer(XibConfiguration)

-(void)setBorderUIColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*)borderUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end