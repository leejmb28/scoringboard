//
//  CALayer+ProxyProperties.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/1/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer(ProxyProperties)

// This assigns a CGColor to borderColor.
@property(nonatomic, assign) UIColor* borderUIColor;

@end