//
//  ListViewController.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/2/10.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController()

@property (nonatomic) NSArray *ListData;
@property (nonatomic) NSArray *PhotoData;

@end

@implementation ListViewController

#if 0
-(void)loadView
{
    
    [super loadView];
    
    const NSInteger numberOfPlayer = 12;
    const NSInteger numberOfInfo   = 14; // Name.Min,Pts.Reb.Ast.Stl.Blk.Tov.
                                         // FTMade.FTTotal.FTAcc.FGMade.FGTotal.FGAcc
    NSMutableArray *playerArray = [NSMutableArray arrayWithCapacity:numberOfPlayer];
    for(NSInteger i=0; i<numberOfPlayer; i++){
        NSMutableArray *infoArray = [NSMutableArray arrayWithCapacity:numberOfInfo];
        for(NSInteger j=0; j<numberOfInfo;j++){
            NSNumber *Data = [NSNumber numberWithFloat:(rand()%10)];
            if((j==9)||(j==12)){
                float madeNumber = [infoArray[j-1] floatValue];
                float totalNumber = 0;
                while(madeNumber>totalNumber)
                    totalNumber = rand()%10;
                Data = [NSNumber numberWithFloat:(madeNumber/totalNumber)];
            }
            [infoArray addObject:Data];
        }
        [playerArray addObject:infoArray];
    }
    self.ListData = [NSArray arrayWithArray:playerArray];
}
#endif

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    const NSInteger numberOfPlayer = 12;
    const NSInteger numberOfInfo   = 5; // Name.Min,Pts.Reb.Ast.Stl.Blk.Tov.
    // FTMade.FTTotal.FTAcc.FGMade.FGTotal.FGAcc
    NSMutableArray *playerArray = [NSMutableArray arrayWithCapacity:numberOfPlayer];
    for(NSInteger i=0; i<numberOfPlayer; i++){
        NSMutableArray *infoArray = [NSMutableArray arrayWithCapacity:numberOfInfo];
        for(NSInteger j=0; j<numberOfInfo;j++){
            NSNumber *Data = [NSNumber numberWithFloat:(rand()%10)];
            #if 0
            if((j==9)||(j==12)){
                float madeNumber = [infoArray[j-1] floatValue];
                float totalNumber = 0;
                while(madeNumber>totalNumber)
                    totalNumber = rand()%10;
                Data = [NSNumber numberWithFloat:(madeNumber/totalNumber)];
            }
            #endif
            [infoArray addObject:Data];
        }
        [playerArray addObject:infoArray];
    }
    self.ListData = [NSArray arrayWithArray:playerArray];
    
    self.PhotoData = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @" ";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
#if 1
    int originx = 90;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    //NSMutableArray *arraylabelkey = [NSMutableArray arrayWithObjects:@"label1", @"label2", @"label3", @"label4", @"label5", nil];
    NSMutableArray *arraylist = [NSMutableArray arrayWithObjects:@"Pts", @"Reb", @"Ast", @"Stl", @"Tov", nil];
    //NSMutableArray *arraylabel = [NSMutableArray array];
    //[dict setObject:arraylabelkey forKey:@"arraylabel"];
    [dict setObject:arraylist forKey:@"arraylist"];
    for(int i=1; i<6; i++){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(originx, 5, 42, 18)];
        //[arraylabel addObject:label];
        originx+=60;
#if 1
        [label setFont:[UIFont boldSystemFontOfSize:12]];
        label.text = [[dict objectForKey:@"arraylist"] objectAtIndex:i-1];
        [view addSubview:label];
#else
        [((UILabel *)[arraylabel objectAtIndex:i-1]) setFont:[UIFont boldSystemFontOfSize:12]];
        ((UILabel *)[arraylabel objectAtIndex:i-1]).text = [[dict objectForKey:@"arraylist"] objectAtIndex:i-1];
        [view addSubview:((UILabel *)[arraylabel objectAtIndex:i-1])];
#endif
    }
#else
    //UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    UILabel *Pts = [[UILabel alloc] initWithFrame:CGRectMake(90, 5, 42, 18)];
    UILabel *Reb = [[UILabel alloc] initWithFrame:CGRectMake(150, 5, 42, 18)];
    UILabel *Ast = [[UILabel alloc] initWithFrame:CGRectMake(210, 5, 42, 18)];
    UILabel *Stl = [[UILabel alloc] initWithFrame:CGRectMake(270, 5, 42, 18)];
    UILabel *Tov = [[UILabel alloc] initWithFrame:CGRectMake(330, 5, 42, 18)];
    
    //[label setFont:[UIFont boldSystemFontOfSize:12]];
    [Pts setFont:[UIFont boldSystemFontOfSize:12]];
    [Reb setFont:[UIFont boldSystemFontOfSize:12]];
    [Ast setFont:[UIFont boldSystemFontOfSize:12]];
    [Stl setFont:[UIFont boldSystemFontOfSize:12]];
    [Tov setFont:[UIFont boldSystemFontOfSize:12]];

    //label.text = [self tableView:tableView titleForHeaderInSection:section];
    Pts.text = @"Pts";
    Reb.text = @"Reb";
    Ast.text = @"Ast";
    Stl.text = @"Stl";
    Tov.text = @"Tov";
    
    //[view addSubview:label];
    //[view addSubview:Pts];
    [view addSubview:Reb];
    [view addSubview:Ast];
    [view addSubview:Stl];
    [view addSubview:Tov];
    [view addSubview:Pts];
#endif
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.PhotoData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float point,rebound,assist,steal,turnover;
    static NSString *ListTableIdentifier = @"ListTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ListTableIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ListTableIdentifier];
    }
    
    UIImageView *photo = (UIImageView *)[cell viewWithTag:100];
    //photo.image = [UIImage imageNamed:self.PhotoData[indexPath.row]];
    photo.image = [UIImage imageNamed:@"WadePhoto.png"];
    
    NSArray *playerInfo = self.ListData[indexPath.row];
    
    UILabel *pt = (UILabel *)[cell viewWithTag:101];
    point = [playerInfo[0] floatValue];
    pt.text = [NSString stringWithFormat:@"%.0f",point];
    
    UILabel *reb = (UILabel *)[cell viewWithTag:102];
    rebound = [playerInfo[1] floatValue];
    reb.text = [NSString stringWithFormat:@"%.0f",rebound];
    
    UILabel *ast = (UILabel *)[cell viewWithTag:103];
    assist = [playerInfo[2] floatValue];
    ast.text = [NSString stringWithFormat:@"%.0f",assist];
    
    UILabel *stl = (UILabel *)[cell viewWithTag:104];
    steal = [playerInfo[3] floatValue];
    stl.text = [NSString stringWithFormat:@"%.0f",steal];
    
    UILabel *tov = (UILabel *)[cell viewWithTag:105];
    turnover = [playerInfo[4] floatValue];
    tov.text = [NSString stringWithFormat:@"%.0f",turnover];

    return cell;
}

@end
