//
//  ScoringBoardViewController.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/1/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "ScoringBoardViewController.h"

@interface ScoringBoardViewController ()

@end

@implementation ScoringBoardViewController
{
    unsigned int scoreA,scoreB,totalsecond;
    NSMutableString *displayString;
    NSTimer *Counter;
}

@synthesize scoringBoardA,scoringBoardB,displayCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    displayString = [NSMutableString stringWithCapacity:10];
    scoreA = scoreB = 0;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",0];
    self.scoringBoardA.text = self.scoringBoardB.text = displayString;
    self.displayCount.text = [NSString stringWithFormat:@"%02i:%02i",0,0];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)plusPointA{
    scoreA++;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",scoreA];
    self.scoringBoardA.text =displayString;
}
-(IBAction)minusPointA{
    if (scoreA == 0) {
        return;
    }
    scoreA--;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",scoreA];
    self.scoringBoardA.text =displayString;
}
-(IBAction)plusPointB{
    scoreB++;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",scoreB];
    self.scoringBoardB.text =displayString;

}
-(IBAction)minusPointB{
    if (scoreB) {
        return;
    }
    scoreB--;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",scoreB];
    self.scoringBoardB.text =displayString;
}
-(IBAction)Start{
    scoreA = scoreB = 0;
    totalsecond = 900;
    displayString = (NSMutableString*)[NSString stringWithFormat:@"%02i",0];
    self.scoringBoardA.text = self.scoringBoardB.text = displayString;
    
    if ([Counter isValid]) {
        [Counter invalidate];
        self.displayCount.text = [NSString stringWithFormat:@"%i:%02i",15,0];
    }
    else{
        Counter = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(MinusSec:) userInfo:nil repeats:YES];
    }
}
-(void)MinusSec:(NSTimer *)sender{
    int minute,second;
    totalsecond--;
    minute = totalsecond/60;
    second = totalsecond%60;
    self.displayCount.text = [NSString stringWithFormat:@"%02i:%02i",minute,second];
    if (totalsecond == 0) {
        [Counter invalidate];
    }
}
@end
