//
//  ListScrollViewController.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/2/21.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListScrollViewController : UIViewController<UIScrollViewDelegate>

@end
